import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import { Events } from 'ionic-angular';

import { ApiService } from '../../providers/apiservice';
import { UserService } from '../../providers/userservice';
import { RECICLAPP, DEBUG } from '../../providers/config';

import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';

@Component({
	selector: 'page-scan',
	templateUrl: 'scan.html'
})
export class ScanPage {

	public scanStatus : boolean = false;

	constructor(public navCtrl: NavController, public events: Events, private qrScanner: QRScanner, public loadingCtrl: LoadingController, private toastCtrl: ToastController, private api: ApiService, private user: UserService) {
		
	}

	ionViewDidEnter(){
		this.scanShow();
		if(this.user.id() > 0){
			this.scan();			
		}		
	}

	ionViewDidLeave() {
		this.scanHide();
	}

	scanShow(){
		window.document.querySelector('ion-app').classList.add('transparentBody');
	}

	scanHide() {
		window.document.querySelector('ion-app').classList.remove('transparentBody');
		this.qrScanner.hide();	
		this.scanStatus = false;
	}

	scanCancel(){
		let toast = this.toastCtrl.create({
			message: 'Has cancelado el escaner',
			duration: 3000,
			position: 'top'
		});
		toast.present();	
		this.scanHide();
		this.navCtrl.parent.select(0); //DashboardPage = 0
	}

	scan() {
		this.qrScanner.prepare().then((status: QRScannerStatus) => {
		    if (status.authorized) {
		    	// start scanning
		    	let scanSub = this.qrScanner.scan().subscribe((text: string) => {
		        	this.qrScanner.hide(); // hide camera preview
		        	scanSub.unsubscribe(); // stop scanning
		        	this.scanHide(); // return background & hide scan
		        	this.scanStatus = false;

	    			let loader = this.loadingCtrl.create({
	      				content: "Comprobando QR ..."
	    			});
	    			loader.present();
		        	let qrText = text.split("::");
		        	let scanValues = { user: this.user.id(), qr: qrText[0] };
			    	let data = JSON.stringify(scanValues);
			    	this.api.postScan(data).then(res=>{
			    		if(res["status"] == 1 && res["data"].length == 1){
			    			let pts = res["data"][0]["curr"];
			    			let total = res["data"][0]["total"];
							let toast = this.toastCtrl.create({
								message: 'Perfecto has sumado '+pts+' puntos con este QR',
								duration: 3000,
								position: 'bottom'
							});
							toast.present();
            				var acumulado = parseInt(pts) + parseInt(RECICLAPP.USER[0].puntos);
							RECICLAPP.USER[0].puntos = acumulado;
							RECICLAPP.USER[0].puntos_actuales = total;
							this.navCtrl.parent.select(0); //DashboardPage = 0
			    		} else {
			    			let errorText = "El código escaneado no es correcto.";
			    			if(res["status"] == 100){
			    				errorText = "Hasta mañana no puedes volver a utilizar este código.";
			    			} else if(res["status"] == 300){
			    				errorText = "Debes estar registrado para poder conseguir puntos.";
			    			}
				        	let toast = this.toastCtrl.create({
				            	message: "ERROR: "+errorText,
				            	duration: 3000,
				            	position: 'top'
				        	});
				        	toast.present();
			    		}
			    		if(DEBUG == true) console.log("scan",res);
			    		loader.dismiss();
			    	}).catch(error => {
				    	alert(error);
				    	loader.dismiss();
				    });
		    	});

		    	this.scanShow();

		    	// show camera preview
		    	this.qrScanner.show();

		    	this.scanStatus = true;

		    	// wait for user to scan something, then the observable callback will be called

		    } else if (status.denied) {
		    	// camera permission was permanently denied
		    	// you must use QRScanner.openSettings() method to guide the user to the settings page
		    	// then they can grant the permission from there
		    	alert('Permiso denegado. Debe dar permisos para hacer fotos y grabar videos.');
		    	this.scanStatus = false;

		    } else {
		    	// permission was denied, but not permanently. You can ask for permission again at a later time.
		    	alert('Permiso denegado. Debe dar permisos para hacer fotos y grabar videos.');
		    	this.scanStatus = false;
		    }
		})
		.catch((e: any) => alert('Has cancelado los permisos para hacer fotos y grabar videos.'));
	}

  	myAccount() {
    	this.events.publish('account:clicked');
  	}

}