import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { App, ViewController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import { HomePage } from '../home/home';

import { ApiService } from '../../providers/apiservice';
import { UserService } from '../../providers/userservice';

import { Storage } from '@ionic/storage';
import { GO_DASHBOARD, RECICLAPP, EMPTY_USER, CURRENT_USER, DEBUG } from '../../providers/config';

@Component({
	selector: 'page-account',
	templateUrl: 'account.html'
})
export class AccountPage {
	tab: string = "profile";
	profile: any;
	movements: any;

	constructor(public navCtrl: NavController, private storage: Storage, public appCtrl: App, public viewCtrl: ViewController, public loadingCtrl: LoadingController, public toastCtrl: ToastController, private alertCtrl: AlertController, public api: ApiService, public user: UserService) {
		this.profile = RECICLAPP.USER[0];

    	let loading = this.loadingCtrl.create({
      		content: 'Cargando datos ...'
    	}); 

		this.api.movements(this.profile.Id).then(res => {
		    if(res["status"] == 1){
		        this.movements = res["data"];

		    } else {
		        if(DEBUG == true) console.log("error api response (movements)");
		        loading.dismiss();
		    }
		});
	}

	resetForm(){
		this.profile.nombre = this.user.name();
		this.profile.apellidos = this.user.surname();
		this.profile.ciudad = this.user.city();
	}

	ionViewDidLeave() {
		this.resetForm();
	}

	ionViewDidEnter(){
		this.resetForm();
	}

	dismiss() {
		this.resetForm();
		this.navCtrl.pop();
	}	

	logout(){
		let alert = this.alertCtrl.create({
			title: 'Cerrar sesión',
			message: '¿Deseas realmente finalizar tu sesión?',
			buttons: [
				{
					text: 'No',
					role: 'cancel',
					handler: () => {
						if(DEBUG == true) console.log('Cancel clicked');
					}
				}, {
					text: 'Sí, cerrar sesión',
					handler: () => {
						RECICLAPP.USER[0] = EMPTY_USER;
						this.storage.remove(GO_DASHBOARD);
						this.storage.remove(CURRENT_USER);
						this.appCtrl.getRootNav().push(HomePage);
						this.viewCtrl.dismiss();
					}
				}
			]
		});
		alert.present();
	}

	save(){
	    if((this.profile.nombre == "")||(this.profile.apellidos == "")||(this.profile.ciudad == "")){
	    	//stop
	    } else {

		    let loader = this.loadingCtrl.create({
		      content: "Comprobando datos ..."
		    });
		    loader.present();

	    	let data = JSON.stringify(this.profile);

	    	this.api.postUpdate(data).then(res=>{
	    		if(res["status"] == 1 && res["data"].length == 1){
	    			RECICLAPP.USER[0] = this.profile;
		        	let toast = this.toastCtrl.create({
		            	message: "Datos actualizados correctamente",
		            	duration: 3000,
		            	position: 'top'
		        	});
		        	toast.present();
		        	this.dismiss();
	    		} else {
		        	let toast = this.toastCtrl.create({
		            	message: "ERROR: Los datos del perfil no se han podido actualizar correctamente.",
		            	duration: 3000,
		            	position: 'bottom'
		        	});
		        	toast.present();
	    		}
	    		loader.dismiss();
	    	}).catch(error => {
		      alert(error);
		      loader.dismiss();
		    });
		}
	}

	civiClub(){
		let url = 'https://www.civiclub.org/es/catalog';
		//window.open(url, '_blank');
		//window.open(url, '_system');
		window.open(url, '_self');
	}

}