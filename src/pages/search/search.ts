import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

import { Events } from 'ionic-angular';

import { RECICLAPP } from '../../providers/config';

import { ResiduePage } from '../residue/residue';
import { MapPage } from '../map/map';

@Component({
	selector: 'page-search',
	templateUrl: 'search.html'
})
export class SearchPage {

  searchTerm: string = '';
  items: any;
  residues: any;
  hasresults: number = 0;

  @ViewChild('search') searchElement;

	constructor(public navCtrl: NavController, public events: Events, public loadingCtrl: LoadingController) {
		
	}

	myAccount() {
  	this.events.publish('account:clicked');
	}

	loadResidues(){
    setTimeout(() => {
      this.searchElement.setFocus();
    },150);
      
		this.residues = RECICLAPP.RESIDUES;
	}

  filterItems(searchTerm){
  	if(searchTerm == ""){
  		this.hasresults = 0;
  		return [];
  	} else {
  		let res = this.residues.filter((item) => {
		  	item.title = item.nombre;
		  	item.id = item.Id;
		  	return item.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
  		});
  		this.hasresults = (res.length > 0) ? res.length : -1;
  		return res;
  	}
	
  }

	setFilteredItems() {
    this.items = this.filterItems(this.searchTerm);
  }

 	ionViewDidLoad(){
    this.loadResidues();
  } 	

  showResidue(rid,lid){
    if(lid > 0){
      this.navCtrl.push(MapPage,{id:rid,idlocal:lid});
    } else {
      this.navCtrl.push(ResiduePage,{id:rid});
    }
  }

}