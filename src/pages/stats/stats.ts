import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { UserService } from '../../providers/userservice';
import { RECICLAPP } from '../../providers/config';

@Component({
	selector: 'page-stats',
	templateUrl: 'stats.html'
})
export class StatsPage {

	stats = [];

	constructor(public navCtrl: NavController, public user: UserService) {
		this.loadStats();
		this.user = RECICLAPP.USER[0];
	}

	loadStats(){
		let usersList = RECICLAPP.USERS;
		usersList = usersList.slice(0, 10);
		let pos = 1;
		let my = 0;
		for(var i in usersList){
			usersList[i].pos = pos;
			usersList[i].active = (usersList[i].Id == this.user.id()) ? true : false;
			if(usersList[i].active == true) usersList[i].puntos = this.user.total();
			this.stats.push(usersList[i]);
			pos++;
			if(usersList[i].active == true) my = 1;
		}
		if(my == 0){
			for(var j in RECICLAPP.USERS){
				if(RECICLAPP.USERS[j].Id == this.user.id()){
					let item = RECICLAPP.USERS[j];
					item.pos = j;
					item.puntos = this.user.total();
					item.pos++;
					item.active = true;
					this.stats.push(item);
					return;
				}
			}
		}
	}

	dismiss() {
		this.navCtrl.pop();
	}
	
}