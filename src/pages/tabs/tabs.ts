import { Component } from '@angular/core';

import { DashboardPage } from '../dashboard/dashboard';
import { SearchPage } from '../search/search';
import { ScanPage } from '../scan/scan';
import { HelpPage } from '../help/help';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = DashboardPage;
  tab2Root = SearchPage;
  tab3Root = ScanPage;
  tab4Root = HelpPage;

  constructor() {

  }

}