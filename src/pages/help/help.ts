import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Events } from 'ionic-angular';

@Component({
	selector: 'page-help',
	templateUrl: 'help.html'
})
export class HelpPage {

	public cities: any;

	constructor(public navCtrl: NavController, public events: Events) {
		
	}

	myAccount() {
  		this.events.publish('account:clicked');
	}

}