import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { LoginPage } from '../login/login';
import { TabsPage } from '../tabs/tabs';

import { GO_DASHBOARD, RECICLAPP, DEBUG } from '../../providers/config';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {

	loginPage = LoginPage;

	dashboardPage = TabsPage;

	constructor(public navCtrl: NavController, private storage: Storage) {
		
	}

	goTo(key) {
		if(key == "login") {
			this.navCtrl.push(this.loginPage, { fromHome: true });
		} else if(key == "dashboard") {
			this.navCtrl.push(this.dashboardPage);
			this.storage.set(GO_DASHBOARD, "go");
		}
	}

}