import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

import { Events } from 'ionic-angular';

import { RECICLAPP } from '../../providers/config';

@Component({
	selector: 'page-point',
	templateUrl: 'point.html'
})
export class PointPage {
	
	point = {};
	local = {};
	city = {};

 	constructor(params: NavParams, public navCtrl: NavController, public events: Events) {
 		
 		this.point = params.get('localPoint');
   		
   		this.local = params.get('localData');

		RECICLAPP.CITIES.filter((item) => {
			if(item.Id == this.point["ciudad"]){
				this.city = item;
				return;
			}
		});
	}

	winPoints(){
		this.dismiss();
		this.events.publish('functionCall:goToWinPoints');
	}

	dismiss() {
		this.navCtrl.pop();
	}
}