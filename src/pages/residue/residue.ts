import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';

import { Events } from 'ionic-angular';

import { ApiService } from '../../providers/apiservice';
import { RECICLAPP, DEBUG } from '../../providers/config';

@Component({
	selector: 'page-residue',
	templateUrl: 'residue.html'
})
export class ResiduePage {
	id: number = 0;
    residue = {};
    container = {};

	constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, private api: ApiService) {
		this.id = navParams.get("id");
		this.stats();
	}

	stats(){
		let statsValues = { "user": RECICLAPP.USER[0].Id, "item": this.id };
    	let data = JSON.stringify(statsValues);
    	this.api.postStats(data).then(res=>{
    		if(res["status"] == 1 && res["data"].length == 1){
    			if(DEBUG == true) console.log("stats ok");
    		} else {
	        	if(DEBUG == true) console.log("stats error");
    		}
    	}).catch(error => {
			if(DEBUG == true) console.log(error);
	    });
	}

  	myAccount(){
    	this.events.publish('account:clicked');
  	}

  	goBack(){
		this.navCtrl.pop();
  	}

  	ionViewDidLoad(){
    	this.loadResidue();
  	}

  	loadResidue(){
		RECICLAPP.RESIDUES.filter((item) => {
			if(item.Id == this.id){
				this.residue = item;
				RECICLAPP.CONTAINERS.filter((cont) => {
					if(item.contenedor == cont.Id){
						this.container = cont;
					}
				});
				return;
			}
		});
  	}

}