import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ModalController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import { Events } from 'ionic-angular';

import { ApiService } from '../../providers/apiservice';
import { UserService } from '../../providers/userservice';
import { RECICLAPP, DEBUG, GMAP } from '../../providers/config';

import { PointPage } from '../point/point';

import { Geolocation } from '@ionic-native/geolocation';
declare var google;

@Component({
	selector: 'page-map',
	templateUrl: 'map.html'
})
export class MapPage {
	id: number = 0;
	idlocal: number = 0;
	cities: any;

    residue = {};
    local = {};
    points = [];
    ciudad: number = 0;

    loading: any;

	@ViewChild('map') mapElement: ElementRef;
	map: any;

	constructor(public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams, public geolocation: Geolocation, public events: Events, public loadingCtrl: LoadingController, private toastCtrl: ToastController, private api: ApiService, private user: UserService) {
		this.id = navParams.get("id");
		this.idlocal = navParams.get("idlocal");
		this.stats();
		this.ciudad = this.user.city();

		this.events.subscribe('functionCall:goToWinPoints', eventData => { 
  			this.navCtrl.parent.select(2); //ScanPage = 2
		});		
	}

	stats(){
		let statsValues = { "user": this.user.id(), "item": this.id };
    	let data = JSON.stringify(statsValues);
    	this.api.postStats(data).then(res=>{
    		if(res["status"] == 1 && res["data"].length == 1){
    			if(DEBUG == true) console.log("stats ok");
    		} else {
	        	if(DEBUG == true) console.log("stats error");
    		}
    	}).catch(error => {
			if(DEBUG == true) console.log(error);
	    });
	}

  	myAccount(){
    	this.events.publish('account:clicked');
  	}

  	goBack(){
		this.navCtrl.pop();
  	}

  	ionViewDidLoad(){
    	this.loadResidue();
    	this.loadLocal();
  	}

  	loadResidue(){
		RECICLAPP.RESIDUES.filter((item) => {
			if(item.Id == this.id){
				this.residue = item;
				return;
			}
		});
  	}

  	loadLocal(){
  		RECICLAPP.LOCALS.filter((item) => {
			if(item.Id == this.idlocal){
				this.local = item;
				this.loadPoints();
				return;
			}
  		});
  	}

  	loadPoints(){
  		RECICLAPP.POINTS.filter((item) => {
			if(item.local == this.local["Id"] && item.ciudad == this.ciudad){
				this.points.push(item);
				return;
			}
  		});
  		if(this.points.length > 0){
  			this.loadMap();
  		} else {
  			this.loadCities();
  		}
  	}

	loadCities(){
		this.cities = RECICLAPP.CITIES;
	}

	changeCity(){
		let cityValues = { "user": this.user.id(), "city": this.ciudad };
    	let data = JSON.stringify(cityValues);
    	this.api.postCity(data).then(res=>{
    		if(res["status"] == 1 && res["data"].length == 1){
    			if(DEBUG == true) console.log("city ok");
    		} else {
	        	if(DEBUG == true) console.log("city error");
    		}
    	}).catch(error => {
			if(DEBUG == true) console.log(error);
	    });
	    let usr = this.user.current();
	    usr.ciudad = this.ciudad;
		this.user.set(usr);
		
		var component = this.navCtrl.getActive().instance;
		if (component.ionViewDidLoad) {
    		component.ionViewDidLoad();
		}
	}

	loadMap(){
		var zoom = GMAP.ZOOM;
		var lat = GMAP.LAT;
		var lng = GMAP.LNG;

		if(this.user.city() > 0){
			for(var j in RECICLAPP.CITIES){
				if(this.user.city() == RECICLAPP.CITIES[j].Id){
					zoom = RECICLAPP.CITIES[j].zoom;
					let geo =  RECICLAPP.CITIES[j].geolocalizacion.split(",");
					lat = geo[0];
					lng = geo[1];
					console.log(zoom, lat, lng);
				}
			}
		}

		zoom = parseInt(zoom);

		let latLng = new google.maps.LatLng(lat, lng);

		let mapOptions = {
			center: latLng,
			zoom: zoom,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}

		this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

		for(var i in this.points){
			this.addMarker(this.points[i]);
		}

		this.loadPosition();
	}

	loadPosition(){

	    this.loading = this.loadingCtrl.create({
	    	content: 'Cargando el mapa ...'
	    }); 

	    this.loading.present();

	    let geoptions = {
	    	"timeout" : 1000 * 10 //10 secs
	    };

		this.geolocation.getCurrentPosition(geoptions).then((position) => {
			let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			
			this.map.setCenter(latLng);
			this.map.setZoom(16);

			var marker = new google.maps.Marker({
				map: this.map,
    			position: this.map.getCenter(),
    			icon: {
      				path: google.maps.SymbolPath.CIRCLE,
      				scale: 5,
      				fillColor: '#0000CC',
      				fillOpacity: 0.75,
      				strokeColor: '#0000CC',
      				strokeWeight: 1
    			}
  			});

			let infoWindow = new google.maps.InfoWindow({
				content: "<b>Mi posición</b>"
			});

			google.maps.event.addListener(marker, "click", () => {
				infoWindow.open(this.map, marker);
			});

			this.loading.dismiss();

		}, (err) => {
	    	this.loading.dismiss();

	    	if(DEBUG == true) console.log(err);

			let toast = this.toastCtrl.create({
				message: 'Lo sentimos pero no podemos determinar tu posición GPS.',
				duration: 3000,
				position: 'top'
			});

			toast.present();
	    });
	}

	addMarker(point){
		let geo = point["geolocalizacion"].split(",");

		let position = new google.maps.LatLng(geo[0], geo[1]);
	 
		let marker = new google.maps.Marker({
			map: this.map,
			animation: google.maps.Animation.DROP,
			position: position
		});
	 	 
		google.maps.event.addListener(marker, "click", () => {
			let profileModal = this.modalCtrl.create(PointPage, { localPoint: point, localData: this.local });
			profileModal.present();
		});
	}
}