import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs';

import { ApiService } from '../../providers/apiservice';

import { Storage } from '@ionic/storage';
import { RECICLAPP, GO_DASHBOARD, CURRENT_USER } from '../../providers/config';

@Component({
	selector: 'page-login',
	templateUrl: 'login.html'
})

export class LoginPage {
	
	fromHome: boolean = false;
	form: any = 'login';
	loginValues = { username:'', password:'' };
	recoveryValues = { username:''};
	registerValues = { username:'', name:'', surname:'', city:'', password:''};

	constructor(params: NavParams, public navCtrl: NavController, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public api: ApiService, private storage: Storage, private alertCtrl: AlertController) {
  		this.fromHome = params.get('fromHome');
	}

	auth(){
	    let loader = this.loadingCtrl.create({
	      content: "Comprobando datos ..."
	    });
	    loader.present();

    	let data = JSON.stringify(this.loginValues);

    	this.api.postLogin(data).then(res=>{
    		if(res["status"] == 1 && res["data"].length == 1){
    			RECICLAPP.USER = res["data"];
    			this.storage.set(CURRENT_USER, RECICLAPP.USER[0].Id);
	        	let toast = this.toastCtrl.create({
	            	message: "Usuario autenticado correctamente. Bienvenido/a "+RECICLAPP.USER[0].nombre+"!!",
	            	duration: 3000
	        	});
	        	toast.present();
	        	this.dismiss();
	        	if(this.fromHome == true){
	        		this.navCtrl.push(TabsPage);
					this.storage.set(GO_DASHBOARD, "go");
	        	}
    		} else {
	        	let toast = this.toastCtrl.create({
	            	message: "El email y/o la contraseña introducidos no son válidos. Inténtalo de nuevo.",
	            	duration: 3000
	        	});
	        	toast.present();		
    		}
    		loader.dismiss();
    	}).catch(error => {
	      alert(error);
	      loader.dismiss();
	    });
	}

	remember() {
		this.form = 'recovery';
	}

	recovery(){
	    let loader = this.loadingCtrl.create({
	      content: "Comprobando email ..."
	    });
	    loader.present();

    	let data = JSON.stringify(this.recoveryValues);

    	this.api.postRecovery(data).then(res=>{
    		if(res["status"] == 1 && res["data"].length == 1){
	        	let toast = this.toastCtrl.create({
	            	message: "Le hemos enviado un email con los pasos a seguir para poder recuperar su contraseña.",
	            	duration: 3000,
	            	position: 'bottom'
	        	});
	        	toast.present();
	        	this.form = 'login';
    		} else {
	        	let toast = this.toastCtrl.create({
	            	message: "ERROR: No existe ninguna cuenta para este correo electrónico. Inténtalo de nuevo.",
	            	duration: 3000,
	            	position: 'top'
	        	});
	        	toast.present();		
    		}
    		loader.dismiss();
    	}).catch(error => {
	      alert(error);
	      loader.dismiss();
	    });
	}

	signin() {
		this.form = 'register';
	}

	register(){
		//check form previously
		let check = "";
		let format = "";
		if(this.registerValues.name == ""){
			check += "<br />· Nombre";
		}
		if(this.registerValues.surname == ""){
			check += "<br />· Apellidos";
		}		
		if(this.registerValues.city == ""){
			check += "<br />· Ciudad";
		}
		if(this.registerValues.username == ""){
			check += "<br />· Email";
		} else if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\D{2,14})+$/.test(this.registerValues.username)){
			format += "<br />· Email";
		}
		if(this.registerValues.password == ""){
			check += "<br />· Contraseña";
		} else if(this.registerValues.password.length < 6 || this.registerValues.password.length > 20){
			format += "<br />· Contraseña (Entre 6 y 20 caracteres)";
		}

		if(check != "" || format != ""){
			let attention = "";
			if(check != ""){
				attention += "Los siguientes campos no pueden estar vacíos:"+"<br />"+check;
			}
			if(format != ""){
				if(attention != "") attention += "<br /><br />";
				attention += "Los siguientes campos contienen errores de formato:"+"<br />"+format;
			}
  			let alert = this.alertCtrl.create({
    			title: 'ATENCIÓN',
    			subTitle: attention,
    			buttons: ['De acuerdo']
  			});
  			alert.present();

		} else {
		    let loader = this.loadingCtrl.create({
		      content: "Enviando datos ..."
		    });
		    loader.present();

	    	let data = JSON.stringify(this.registerValues);

	    	this.api.postRegister(data).then(res=>{
	    		if(res["status"] == 1 && res["data"].length == 1){
		        	let toast = this.toastCtrl.create({
		            	message: "Le hemos enviado un email con los pasos a seguir para poder confirmar la cuenta.",
		            	duration: 3000,
		            	position: 'bottom'
		        	});
		        	toast.present();
		        	this.form = 'login';
	    		} else {
	    			let errorRegister = "";
	    			if(res["status"] == 200){
	    				errorRegister = "ERROR: El email introducido ya existe en el sistema. Inténtalo de nuevo.";
	    			} else {
	    				errorRegister = "ERROR: Ha ocurrido un error desconocido en el servidor. Inténtalo de nuevo.";
	    			}
		        	let toast = this.toastCtrl.create({
		            	message: errorRegister,
		            	duration: 3000,
		            	position: 'top'
		        	});
		        	toast.present();		
	    		}
	    		loader.dismiss();
	    	}).catch(error => {
		      alert(error);
		      loader.dismiss();
		    });
		}
	}

	back() {
		this.form = 'login';
	}

	dismiss() {
		this.navCtrl.pop();
	}

}