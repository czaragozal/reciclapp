import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';

import { Events } from 'ionic-angular';

import { StatsPage } from '../stats/stats';

import { UserService } from '../../providers/userservice';
import { RECICLAPP } from '../../providers/config';

@Component({
	selector: 'page-dashboard',
	templateUrl: 'dashboard.html'
})
export class DashboardPage {
	stats = [];
	users = [];
	top: number = 3;
	pos: number = 0;

	constructor(public modalCtrl: ModalController, public navCtrl: NavController, public events: Events, public loadingCtrl: LoadingController, public user: UserService) {
		this.loadStats();
	}

  	public getPuntos(){
    	return this.user.points();
  	}

	loadStats(){
		let usersList = RECICLAPP.USERS;
		usersList = usersList.slice(0, this.top);
		this.pos = 1;
		for(var i in usersList){
			usersList[i].pos = this.pos;
			this.users.push(usersList[i]);
			this.pos++;
		}

		let seachList = RECICLAPP.STATS;
		seachList = seachList.slice(0, this.top);
		this.pos = 1;
		for(var j in seachList){
			seachList[j].pos = this.pos;
			this.stats.push(seachList[j]);
			this.pos++;
		}		
	}

  	goToStats() {
		let statsModal = this.modalCtrl.create(StatsPage);
		statsModal.present();  	
  	}

  	goToRecycle() {
    	this.navCtrl.parent.select(1); //SearchPage = 1
  	}

  	myAccount() {
    	this.events.publish('account:clicked');
  	}
	
}