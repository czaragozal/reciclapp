import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import { Headers } from '@angular/http';

import 'rxjs/add/operator/map';

import { API_URL, DEBUG } from './config';

@Injectable()
export class ApiService {

  constructor(private http: Http) {
    
  }

  public postRegister(data){
    return new Promise(resolve => {
      this.http.post(API_URL + "/register", data).map(res => res.json()).subscribe(data => {
        resolve(data);
        if(DEBUG == true) console.log("register", data);
      });
    });
  }

  public postRecovery(data){
    return new Promise(resolve => {
      this.http.post(API_URL + "/recovery", data).map(res => res.json()).subscribe(data => {
        resolve(data);
        if(DEBUG == true) console.log("recovery", data);
      });
    });
  }

  public postUpdate(data){
    return new Promise(resolve => {
      this.http.post(API_URL + "/update", data).map(res => res.json()).subscribe(data => {
        resolve(data);
        if(DEBUG == true) console.log("update", data);
      });
    });
  }

  public postCity(data){
    return new Promise(resolve => {
      this.http.post(API_URL + "/city", data).map(res => res.json()).subscribe(data => {
        resolve(data);
        if(DEBUG == true) console.log("city", data);
      });
    });  
  }

  public postStats(data){
    return new Promise(resolve => {
      this.http.post(API_URL + "/stats", data).map(res => res.json()).subscribe(data => {
        resolve(data);
        if(DEBUG == true) console.log("stats", data);
      });
    });  
  }

  public postLogin(data){
    return new Promise(resolve => {
      this.http.post(API_URL + "/login", data).map(res => res.json()).subscribe(data => {
        resolve(data);
        if(DEBUG == true) console.log("login", data);
      });
    });
  }

  public postScan(data){
    return new Promise(resolve => {
      this.http.post(API_URL + "/scan", data).map(res => res.json()).subscribe(data => {
        resolve(data);
        if(DEBUG == true) console.log("scan", data);
      });
    });
  }

  public movements(usr) {
    return new Promise(resolve => {
      this.http.get(API_URL + "/acciones_saldo/"+usr+"?order=fecha:desc").map(res => res.json()).subscribe(data => {
        if(data["status"] == 1){
          let items = [];
          for(var i in data["data"]){
            if(data["data"][i]["estado"] == 1){
              items.push(data["data"][i]);
            }
          }
          data["data"] = items;
        }
        resolve(data);
        if(DEBUG == true) console.log("movements", data);
      })
    });
  }

  public cities() {
    return new Promise(resolve => {
      this.http.get(API_URL + "/ciudades").map(res => res.json()).subscribe(data => {
        if(data["status"] == 1){
          let items = [];
          for(var i in data["data"]){
            if(data["data"][i]["estado"] == 1){
              items.push(data["data"][i]);
            }
          }
          data["data"] = items;
        }
        resolve(data);
        if(DEBUG == true) console.log("cities", data);
      })
    });
  }

  public residues() {
    return new Promise(resolve => {
      this.http.get(API_URL + "/residuos?order=nombre").map(res => res.json()).subscribe(data => {
        if(data["status"] == 1){
          let items = [];
          for(var i in data["data"]){
            if(data["data"][i]["estado"] == 1 && (data["data"][i]["contenedor"]>0 || data["data"][i]["local"]>0)){
              items.push(data["data"][i]);
            }
          }
          data["data"] = items;
        }
        resolve(data);
        if(DEBUG == true) console.log("residues", data);
      })
    });
  }

  public containers() {
    return new Promise(resolve => {
      this.http.get(API_URL + "/contenedores").map(res => res.json()).subscribe(data => {
        if(data["status"] == 1){
          let items = [];
          for(var i in data["data"]){
            if(data["data"][i]["estado"] == 1){
              items.push(data["data"][i]);
            }
          }
          data["data"] = items;
        }        
        resolve(data);
        if(DEBUG == true) console.log("containers", data);
      })
    });
  }

  public locals() {
    return new Promise(resolve => {
      this.http.get(API_URL + "/locales").map(res => res.json()).subscribe(data => {
        if(data["status"] == 1){
          let items = [];
          for(var i in data["data"]){
            if(data["data"][i]["estado"] == 1){
              items.push(data["data"][i]);
            }
          }
          data["data"] = items;
        }
        resolve(data);
        if(DEBUG == true) console.log("locals", data);
      })
    });
  }

  public points() {
    return new Promise(resolve => {
      this.http.get(API_URL + "/puntos").map(res => res.json()).subscribe(data => {
        if(data["status"] == 1){
          let items = [];
          for(var i in data["data"]){
            if(data["data"][i]["estado"] == 1){
              items.push(data["data"][i]);
            }
          }
          data["data"] = items;
        }
        resolve(data);
        if(DEBUG == true) console.log("points", data);
      })
    });
  }

  public stats() {
    return new Promise(resolve => {
      this.http.get(API_URL + "/estadisticas").map(res => res.json()).subscribe(data => {
        if(data["status"] == 1){
          let items = [];
          for(var i in data["data"]){
            items.push(data["data"][i]);
          }
          data["data"] = items;
        }
        resolve(data);
        if(DEBUG == true) console.log("stats", data);
      })
    });
  }

  public users() {
    return new Promise(resolve => {
      this.http.get(API_URL + "/usuarios?order=puntos:desc").map(res => res.json()).subscribe(data => {
        if(data["status"] == 1){
          let items = [];
          for(var i in data["data"]){
            if(data["data"][i]["estado"] == 1){
              items.push(data["data"][i]);
            }
          }
          data["data"] = items;
        }
        resolve(data);
        if(DEBUG == true) console.log("users", data);
      })
    });
  }

}