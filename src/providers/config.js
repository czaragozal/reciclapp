export const API_URL = "https://cristobal.dommia.es/api/rest-api.php";

export const GO_DASHBOARD = "first-to-dashboard";
export const CURRENT_USER = "current-user";
export const LAST_CITY = "last-city";

export const DEBUG = true;

export var EMPTY_USER = {"Id":0, "ciudad":0, "puntos":0, "puntos_actuales":0, "nombre":"", "apellidos":"", "email":""};

export var GMAP = {
	LAT: "40.309390", 
	LNG: "-3.684106",
	ZOOM: 5
};

export var RECICLAPP = {
	CITIES: [],
	RESIDUES: [],
	LOCALS: [],
	POINTS: [],
	USERS: [],
	STATS: [],
	USER: [EMPTY_USER]
};