import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';
import { RECICLAPP, LAST_CITY } from './config';

@Injectable()
export class UserService {

  constructor(private storage: Storage) {
    
  }

  public current(){
  	return RECICLAPP.USER[0];
  }

  public city(){
    return RECICLAPP.USER[0].ciudad;
  }

  public name(){
    return RECICLAPP.USER[0].nombre;
  }

  public surname(){
    return RECICLAPP.USER[0].apellidos;
  }

  public points(){
    return RECICLAPP.USER[0].puntos_actuales;
  }

  public total(){
    return RECICLAPP.USER[0].puntos;
  }

  public id(){
  	return RECICLAPP.USER[0].Id;
  }

  public set(user){
    this.storage.set(LAST_CITY, user.ciudad);
    RECICLAPP.USER[0] = user;
  }

}