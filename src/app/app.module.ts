import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { HttpModule } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';

import { MyApp } from './app.component';

import { ApiService } from '../providers/apiservice';
import { UserService } from '../providers/userservice';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { SearchPage } from '../pages/search/search';
import { ScanPage } from '../pages/scan/scan';
import { ResiduePage } from '../pages/residue/residue';
import { MapPage } from '../pages/map/map';
import { PointPage } from '../pages/point/point';
import { StatsPage } from '../pages/stats/stats';
import { AccountPage } from '../pages/account/account';
import { HelpPage } from '../pages/help/help';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    DashboardPage,
    TabsPage,
    SearchPage,
    ScanPage,
    ResiduePage,
    MapPage,
    PointPage,
    StatsPage,
    AccountPage,
    HelpPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    DashboardPage,
    TabsPage,
    SearchPage,
    ScanPage,
    ResiduePage,
    MapPage,
    PointPage,
    StatsPage,
    AccountPage,
    HelpPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiService,
    UserService,
    Storage,
    Geolocation,
    QRScanner 
  ]
})

export class AppModule {}