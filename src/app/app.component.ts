import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoadingController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { AccountPage } from '../pages/account/account';

import { Events } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { GO_DASHBOARD, RECICLAPP, CURRENT_USER, DEBUG, LAST_CITY } from '../providers/config';

import { ApiService } from '../providers/apiservice';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any = HomePage;

  loading: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public events: Events, private storage: Storage, public loadingCtrl: LoadingController, public api: ApiService, public modalCtrl: ModalController) {
    splashScreen.show();

    this.loading = this.loadingCtrl.create({
      content: 'Cargando residuos, contenedores, ciudades, locales ...'
    }); 

    this.loading.present();
    
    platform.ready().then(() => {
      //splashScreen.hide();

      this.loadResources();

      this.events.subscribe('account:clicked', (item) => {
        let loginModal: any;
        if(RECICLAPP.USER[0].Id == 0){
          loginModal = this.modalCtrl.create(LoginPage);
        } else {
          loginModal = this.modalCtrl.create(AccountPage);
        }
        loginModal.present();
      });

    	statusBar.styleDefault();
    });
  }

  loadResources() {
    this.loadCities();
  }

  loadCities(){
    this.api.cities().then(res => {
      if(res["status"] == 1){
        RECICLAPP.CITIES = res["data"];
        this.loadResidues();

      } else {
        if(DEBUG == true) console.log("error api response (loadCities)");
        this.loading.dismiss();
      }
    });
  }

  loadResidues(){
    this.api.residues().then(res => {
      if(res["status"] == 1){
        RECICLAPP.RESIDUES = res["data"];
        this.loadContainers();

      } else {
        if(DEBUG == true) console.log("error api response (loadResidues)");
        this.loading.dismiss();
      }
    });
  }  

  loadContainers(){
    this.api.containers().then(res => {
      if(res["status"] == 1){
        RECICLAPP.CONTAINERS = res["data"];
        this.loadLocals();

      } else {
        if(DEBUG == true) console.log("error api response (loadContainers)");
        this.loading.dismiss();
      }
    });
  }

  loadLocals(){
    this.api.locals().then(res => {
      if(res["status"] == 1){
        RECICLAPP.LOCALS = res["data"];
        this.loadPoints();

      } else {
        if(DEBUG == true) console.log("error api response (loadLocals)");
        this.loading.dismiss();
      }
    });
  }

  loadPoints(){
    this.api.points().then(res => {
      if(res["status"] == 1){
        RECICLAPP.POINTS = res["data"];
        this.loadUsers();

      } else {
        if(DEBUG == true) console.log("error api response (loadPoints)");
        this.loading.dismiss();
      }
    });
  }

  loadUsers(){
    this.api.users().then(res => {
      if(res["status"] == 1){
        RECICLAPP.USERS = res["data"];
        this.loadStats();

      } else {
        if(DEBUG == true) console.log("error api response (loadUsers)");
        this.loading.dismiss();
      }
    });
  }

  loadStats(){
    this.api.stats().then(res => {
      if(res["status"] == 1){
        RECICLAPP.STATS = res["data"];
        this.loadUser();

      } else {
        if(DEBUG == true) console.log("error api response (loadStats)");
        this.loading.dismiss();
      }
    });
  }

  loadUser(){
    this.storage.get(CURRENT_USER).then((value) => {
      if(value){
        for(var i in RECICLAPP.USERS){
          if(RECICLAPP.USERS[i]["Id"] == value){
            RECICLAPP.USER[0] = RECICLAPP.USERS[i];
          }
        }
        this.loadCity();
      } else {
        this.loadCity();
      }
    });
  }

  loadCity(){
    if(RECICLAPP.USER[0].Id == 0){
      this.storage.get(LAST_CITY).then((value) => {
        if(value){
          console.log("last city", value);
          RECICLAPP.USER[0].ciudad = value;
          this.loadEnd();
        } else {
          this.loadEnd();
        }
      });
    } else {
      this.loadEnd();
    }
  }

  loadEnd(){
    this.storage.get(GO_DASHBOARD).then((value) => {
      if(value){
        this.rootPage = TabsPage;
      }
    });
    this.loading.dismiss();
  }

}